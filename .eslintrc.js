const path = require('path');
const paths = require('./config/paths');

const resolveFile = (filePath) => {
  try {
    return require.resolve(path.resolve(process.cwd(), filePath));
  } catch (e) { }
};

const retrieveConfig = (baseConfig) => {
  const customConfigFile = paths.eslintConfig && resolveFile(paths.eslintConfig);

  if (customConfigFile) {
    const customConfig = require(customConfigFile);
    if (typeof customConfig === 'function') {
      return customConfig(baseConfig);
    } else {
      return Object.assign({}, baseConfig, customConfig);
    }
  }

  return baseConfig;
};

// Do not add "rules" here; add theme to eslint-config-amazee.
module.exports = retrieveConfig({
  extends: '@amazee/eslint-config-amazee',
  settings: {
    'import/resolver': {
      webpack: {
        config: require.resolve(
          `${__dirname}/config/webpack/webpack.resolve.js`
        ),
      },
    },
  },
});
