module.exports = (function () {
  if (global.Drupal) {
    return global.Drupal;
  }

  if (__DEVELOPMENT__) {
    if (global.location.hostname === 'localhost') {
      return {
        t: function (string, args) {
          for (var key in args) {
            string = string.replace(key, args[key]);
          }

          return string;
        },
      };
    }
  }

  return {};
})();
