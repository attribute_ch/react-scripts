const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const paths = require('../paths');
const ExtractCssChunks = require('extract-css-chunks-webpack-plugin');

const noCache = require('require-no-cache');
const postCssCalc = require('postcss-calc');
const postCssNext = require('postcss-cssnext');
const postCssNested = require('postcss-nested');
const postCssFocus = require('postcss-focus');
const postCssReporter = require('postcss-reporter');
const postCssVars = require('postcss-simple-vars');
const postCssColorFunction = require('postcss-color-function');
const postCssImport = require('postcss-import');
const postCssForLoop = require('postcss-for');
const postCssMixins = require('postcss-mixins');
const postCssFunctions = require('postcss-functions');

const postCssVariables = path.join(paths.appSrc, 'shared', 'css', 'variables.js');
const postCssPlugins = (loader) => ([
  postCssImport({ root: loader.resourcePath }),
  postCssVars({
    variables: () => {
      if (fs.existsSync(postCssVariables)) {
        return noCache(postCssVariables);
      }

      return {};
    },
  }),
  postCssForLoop(),
  postCssMixins({
    mixinsDir: path.join(paths.appSrc, 'shared', 'css', 'mixins'),
  }),
  postCssFunctions({
    glob: path.join(paths.appSrc, 'shared', 'css', 'functions', '*.js'),
  }),
  postCssFocus(), // Add a :focus to every :hover
  postCssColorFunction(),
  postCssCalc(),
  postCssNext({ // Allow future CSS features to be used, also auto-prefixes the CSS.
    browsers : ['last 2 versions', 'IE > 10'],
  }),
  postCssNested(),
  postCssReporter({ // Posts messages from plugins to the terminal
    clearMessages : true,
  }),
]);

module.exports = (env, type, options) => {
  const server = type === 'server';
  const prod = env === 'prod';

  const rules = [{
    test: /\.js$/, // Transform all .js files required somewhere with Babel.
    include: paths.appSrc,
    use: [
      {
        loader: 'babel-loader',
        options: {
          babelrc: false,
          extends: require.resolve('../../.babelrc'),
          // This is a feature of `babel-loader` for webpack (not Babel itself).
          // It enables caching results in ./node_modules/.cache/babel-loader/
          // directory for faster rebuilds.
          cacheDirectory: !prod,
        },
      },
    ],
  }, {
    test: /\.css$/,
    use: ((config) => server ? config : ExtractCssChunks.extract({
      use: config,
    }))([{
      loader: server ? 'css-loader/locals' : 'css-loader',
      options: {
        modules: true,
        importLoaders: 1,
        localIdentName: prod ? paths.cssIdentNameProd : paths.cssIdentNameDev,
        sourceMap: !prod,
      },
    }, {
      loader: 'postcss-loader',
      options: {
        ident: 'postcss',
        plugins: postCssPlugins,
        sourceMap: !prod,
      },
    }]),
  }, {
    test: /\.json($|\?)/,
    use: ['json-loader'],
  }, {
    test: /\.(jpe?g|png|gif)$/,
    use: server ? ['file-loader?emitFile=false'] : ['file-loader'],
  }, {
    test: /\.(eot|svg|ttf|woff|woff2)$/,
    use: server ? ['file-loader?emitFile=false'] : ['file-loader'],
  }, {
    test: /\.(gql|graphql)$/,
    use: ['graphql-tag/loader'],
  }].concat(options.rules || []);

  // Use the file-loader for any other file types.
  const existingTests = rules.map(rule => rule.test).filter(test => !!test);
  const rulesWithFallback = rules.concat([{
    test: {
      not: existingTests,
    },
    use: server ? ['file-loader?emitFile=false'] : ['file-loader'],
  }]);

  return {
    name: options.name || undefined,
    externals: options.externals || undefined,
    entry: options.entry,
    context: options.context || path.resolve(process.cwd(), 'app'),
    output: options.output, // Merge with env dependent settings.
    module: {
      rules: rulesWithFallback,
    },
    plugins: (options.plugins || []).concat([
      // TODO: Remove temporary solution for https://github.com/andris9/encoding/issues/16#issuecomment-226996274
      new webpack.NormalModuleReplacementPlugin(/iconv-loader/, 'node-noop'),
      new webpack.ProvidePlugin({
        Drupal: require.resolve('../mock/drupal'),
      }),
      new webpack.DefinePlugin({
        __CLIENT__: !server,
        __SERVER__: server,
        __PRODUCTION__: prod,
        __DEVELOPMENT__: !prod,
      }),
    ], server ? [
      new webpack.optimize.LimitChunkCountPlugin({
        // Disable code splitting on the server.
        maxChunks: 1,
      }),
    ] : [
      new ExtractCssChunks(),   
    ]),
    devtool: options.devtool,
    target: options.target || 'web', // Make web variables accessible to webpack, e.g. window.
    stats: false, // Don't show stats in the console.
    resolve: Object.assign({}, require('./webpack.resolve').resolve, {
      mainFields: options.resolveMainFields || ['browser', 'module', 'main'],
    }),
  };
};
