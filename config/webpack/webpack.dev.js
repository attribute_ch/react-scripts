const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const WatchMissingNodeModulesPlugin = require('react-dev-utils/WatchMissingNodeModulesPlugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const HappyPack = require('happypack');
const createWebpackConfig = require('./webpack.base');
const paths = require('../paths');

const dotEnv = require('dotenv-extended').config({
  // We are already loading the config in the build script.
  assignToProcessEnv: false,
});

const dotEnvStringifed = Object.keys(dotEnv).reduce(
  (carry, current) =>
    Object.assign(carry, {
      [current]: JSON.stringify(dotEnv[current]),
    }),
  {}
);

const commonPlugins = [
  new HappyPack({
    loaders: [
      {
        path: 'babel-loader',
        query: {
          babelrc: false,
          extends: require.resolve('../../.babelrc'),
          // This is a feature of `babel-loader` for webpack (not Babel itself).
          // It enables caching results in ./node_modules/.cache/babel-loader/
          // directory for faster rebuilds.
          cacheDirectory: true,
        },
      },
    ],
  }),
  // Always expose NODE_ENV to webpack, in order to use `process.env.NODE_ENV`
  // inside your code for any environment checks; UglifyJS will automatically
  // drop any unreachable code.
  new webpack.DefinePlugin({
    'process.env': Object.assign({}, dotEnvStringifed, {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
    }),
  }),
  new CaseSensitivePathsPlugin(),
  new WatchMissingNodeModulesPlugin(),
];

const clientConfig = createWebpackConfig('dev', 'client', {
  devtool: 'eval-source-map',
  name: 'client',
  // Add hot reloading in development.
  entry: [
    'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=false&quiet=false&noInfo=false',
    'react-hot-loader/patch',
    // We ship a few polyfills by default.
    require.resolve('../polyfills'),
    paths.appEntryClient || paths.appEntry,
  ],
  // Don't use hashes in dev mode for better performance.
  output: {
    // This does not produce a real file. It's just the virtual path that is
    // served from memory.
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].js',
    // Add /* filename */ comments to generated require()s in the output.
    pathinfo: true,
    path: paths.appBuild,
    publicPath: '/',
  },
  plugins: commonPlugins.concat([
    new webpack.optimize.CommonsChunkPlugin({
      names: ['bootstrap'], // needed to put webpack bootstrap code before chunks
      filename: '[name].js',
      minChunks: Infinity,
    }),
    new webpack.HotModuleReplacementPlugin(),
  ]),
});

// if you're specifying externals to leave unbundled, you need to tell Webpack
// to still bundle `react-universal-component`, `webpack-flush-chunks` and
// `require-universal-module` so that they know they are running
// within Webpack and can properly make connections to client modules:
const modeModules = path.resolve(process.cwd(), 'node_modules');
const externals = fs
  .readdirSync(modeModules)
  .filter(x => !/\.bin|react-universal-component|require-universal-module|webpack-flush-chunks/.test(x))
  .reduce((externals, mod) => {
    externals[mod] = `commonjs ${mod}`;
    return externals;
  }, {});

const serverConfig = createWebpackConfig('dev', 'server', {
  devtool: 'eval',
  name: 'server',
  target: 'node',
  resolveMainFields: ['node', 'module', 'main'],
  entry: [
    // We ship a few polyfills by default.
    require.resolve('../polyfills'),
    paths.appEntryServer,
  ],
  output: {
    filename: 'server.js',
    libraryTarget: 'commonjs2',
    path: path.resolve(process.cwd(), 'build'),
    publicPath: paths.publicPath,
  },
  externals,
  plugins: commonPlugins,
});

module.exports = [clientConfig, serverConfig];
