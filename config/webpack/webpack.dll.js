/**
 * webpack DLL config
 *
 * This configuration is used to cache webpack's module
 * contexts for external library and framework type
 * dependencies which will usually not change often enough
 * to warrant building them from scratch every time we use
 * the webpack process.
 *
 * Use with a configuration entry in package.json such as
 * the following:
 *
 * "dllPlugin": {
 *   "path": "build", // Path for built DLL files
 *   "dlls": {
 *     "vendor": // This will be the name of the built DLL file
 *     [
 *       // Include external libs here...
 *       "@amazee/paraplegie/lib/js/app/modules/cms/ImageSlider.js",
 *       "@amazee/react-scripts/config/polyfills.js",
 *       "react",
 *       "react-dom",
 *       "redux"
 *     ]
 *   }
 * }
 *
 * To compile a list of dependencies for the array in the
 * configuration, the dependencies can be analysed by
 * viewing the full webpack-dev-server output with the
 * `stats.maxModules` option:
 * https://gist.github.com/karlhorky/36407633411026275acdee51440d5053
 *
 * All library entries not marked with `delegated` as
 * follows are good candidates for inclusion in the DLL.
 * Example output:
 *
 * [106] delegated ./node_modules/react-dom/index.js from dll-reference vendor 42 bytes {5} [not cacheable] [built]
 *
 */

const { join } = require('path');
const webpack = require('webpack');
const paths = require('../paths');

// These checks are also in webpack.dev.js
// https://github.com/AmazeeLabs/amazee-js/commit/218e436fc120369f878a74be6811060711c14b32
if (!paths.dllPlugin || !paths.dllPlugin.dlls) {
  process.exit(0);
}

module.exports = {
  context: process.cwd(),
  entry: paths.dllPlugin.dlls,
  devtool: '#source-map',
  output: {
    filename: '[name].dll.js',
    path: paths.dllPluginOutputPath,
    library: '[name]',
  },
  plugins: [
    new webpack.DllPlugin({
      name: '[name]',
      path: join(paths.dllPluginOutputPath, '[name].json'),
    }),
  ],
};
