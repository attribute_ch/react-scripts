const path = require('path');

module.exports = {
  resolve: {
    alias: {
      // Resolve Babel runtime relative to react-scripts.
      // It usually still works on npm 3 without this but it would be
      // unfortunate to rely on, as react-scripts could be symlinked,
      // and thus babel-runtime might not be resolvable from the source.
      'babel-runtime': path.dirname(
        require.resolve('babel-runtime/package.json')
      ),
    },
    modules: ['screens', 'components', 'shared', 'node_modules'],
    extensions: ['.js'],
  },
};
