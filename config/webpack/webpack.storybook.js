const amazeeDefaults = require('./webpack.base');
const path = require('path');
const webpack = require('webpack');
const paths = require('../paths');

module.exports = (config) => {
  const amazee = amazeeDefaults('dev', 'client', {});

  return Object.assign({}, config, {
    resolve: Object.assign({}, config.resolve, {
      modules: amazee.resolve.modules,
    }),
    module: Object.assign({}, config.module, {
      rules: config.module.rules.filter((item) => {
        // Remove the default css loader from storybook.
        return !item.test || (item.test + '').indexOf('.css') === -1;
      }).concat(amazee.module.rules.filter((item) => {
        // Add the css loader from our custom config.
        return item.test && (item.test + '').indexOf('.css') !== -1;
      })),
    }),
    plugins: config.plugins.concat(amazee.plugins),
  });
};
