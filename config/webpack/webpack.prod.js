const path = require('path');
const webpack = require('webpack');
const StatsPlugin = require('stats-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const paths = require('../paths');
const createWebpackConfig = require('./webpack.base');

const clientConfig = createWebpackConfig('prod', 'client', {
  devtool: 'source-map',
  name: 'client',
  entry: [
    // We ship a few polyfills by default.
    require.resolve('../polyfills'),
    paths.appEntryClient || paths.appEntry,
  ],
  // Utilize long-term caching by adding content hashes (not compilation hashes)
  // to compiled assets.
  output: {
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].js',
    pathinfo: true,
    path: path.join(paths.appBuild, 'public'),
    publicPath: paths.publicPath,
  },
  plugins: [
    new StatsPlugin('../stats.json'),
    // OccurrenceOrderPlugin is needed for long-term caching to work properly.
    new webpack.optimize.OccurrenceOrderPlugin(true),
    // Minify and optimize the JavaScript.
    new webpack.optimize.UglifyJsPlugin({
      output: {
        screw_ie8: true,
        comments: false
      },
      compress: {
        warnings: false,
        screw_ie8: true,
      },
      mangle: {
        screw_ie8: true,
        except: ['Drupal'],
      },
      sourceMap: true,
    }),
    new webpack.optimize.CommonsChunkPlugin({
      names: ['bootstrap'], // needed to put webpack bootstrap code before chunks
      filename: '[name].[chunkhash].js',
      minChunks: Infinity,
    }),
    new WebpackMd5Hash(),
    new webpack.NoEmitOnErrorsPlugin(),
    // Always expose NODE_ENV to webpack, in order to use `process.env.NODE_ENV`
    // inside your code for any environment checks; UglifyJS will automatically
    // drop any unreachable code.
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'production'),
      },
    }),
  ],
});

const serverConfig = createWebpackConfig('prod', 'server', {
  devtool: 'source-map',
  name: 'server',
  target: 'node',
  resolveMainFields: ['node', 'module', 'main'],
  entry: [
    // We ship a few polyfills by default.
    require.resolve('../polyfills'),
    paths.appEntryServer,
  ],
  output: {
    filename: 'server.js',
    libraryTarget: 'commonjs2',
    path: path.resolve(process.cwd(), 'build'),
    publicPath: paths.publicPath,
  },
  plugins: [
    new webpack.optimize.LimitChunkCountPlugin({
      // Disable code splitting on the server.
      maxChunks: 1,
    }),
    // Always expose NODE_ENV to webpack, in order to use `process.env.NODE_ENV`
    // inside your code for any environment checks; UglifyJS will automatically
    // drop any unreachable code.
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'production'),
      },
    }),
  ],
});

module.exports = [clientConfig, serverConfig];
