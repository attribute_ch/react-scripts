#!/usr/bin/env node

var spawn = require('cross-spawn');
var script = process.argv[2];
var args = process.argv.slice(3);
var file;

try {
  file = require.resolve('../scripts/' + script);
}
catch (e) {
  console.log('Unknown script "' + script + '".');
  console.log('Perhaps you need to update @amazee/react-scripts?');
  process.exit(1);
}

var result = spawn.sync('node', [file].concat(args), { stdio: 'inherit' });
process.exit(result.status);
