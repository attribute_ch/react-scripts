process.env.NODE_ENV = 'development';

// Load environment variables from .env file.
require('dotenv-extended').config();

const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackHotServerMiddleware = require('webpack-hot-server-middleware');
const express = require('express');
const chalk = require('chalk');
const webpack = require('webpack');
const detect = require('detect-port');
const inquirer = require('react-dev-utils/inquirer');
const clearConsole = require('react-dev-utils/clearConsole');
const formatWebpackMessages = require('react-dev-utils/formatWebpackMessages');
const getProcessForPort = require('react-dev-utils/getProcessForPort');
const openBrowser = require('react-dev-utils/openBrowser');
const paths = require('../config/paths');
const webpackOptions = require('../config/webpack/webpack.dev');
const interactive = !!process.stdout.isTTY;

const setupCompiler = () => {
  const webpackCompiler = webpack(webpackOptions);

  // "invalid" event fires when you have changed a file, and Webpack is
  // recompiling a bundle. WebpackDevServer takes care to pause serving the
  // bundle, so if you refresh, it'll wait instead of serving the old one.
  // "invalid" is short for "bundle invalidated", it doesn't imply any errors.
  webpackCompiler.plugin('invalid', () => {
    if (interactive) {
      clearConsole();
    }

    console.log('Compiling ...');
  });

  // "done" event fires when Webpack has finished recompiling the bundle.
  // Whether or not you have warnings or errors, you will get this event.
  webpackCompiler.plugin('done', (stats) => {
    if (interactive) {
      clearConsole();
    }

    // We have switched off the default Webpack output in WebpackDevServer
    // options so we are going to "massage" the warnings and errors and present
    // them in a readable focused way.
    const messages = formatWebpackMessages(stats.toJson({}, true));
    const successful = !messages.errors.length && !messages.warnings.length;

    if (successful) {
      console.log(chalk.green('Compiled successfully.'));
    }

    // If errors exist, only show errors.
    if (messages.errors.length) {
      console.log(chalk.red('Failed to compile.'));
      console.log();

      messages.errors.forEach((message) => {
        console.log(message);
        console.log();
      });

      return;
    }

    // Show warnings if no errors were found.
    if (messages.warnings.length) {
      console.log(chalk.yellow('Compiled with warnings.'));
      console.log();

      messages.warnings.forEach((message) => {
        console.log(message);
        console.log();
      });

      // Teach some ESLint tricks.
      console.log('You may use special comments to disable some warnings.');
      console.log('Use ' + chalk.yellow('// eslint-disable-next-line') + ' to ignore the next line.');
      console.log('Use ' + chalk.yellow('/* eslint-disable */') + ' to ignore all warnings in a file.');
    }
  });

  return webpackCompiler;
};

const runDevServer = (port, compiler) => {
  const app = express();

  app.set('port', port);
  app.set('paths', paths);
  app.set('env', process.env);

  const clientCompiler = compiler.compilers && compiler.compilers.find((item) => item.name === 'client') || compiler;
  app.use(webpackDevMiddleware(compiler, {
    publicPath: paths.appPublic,
    quiet: true,
  }));

  app.use(webpackHotMiddleware(clientCompiler));

  // Add server side hot reloading if there is a server entry point.
  if (compiler.compilers && compiler.compilers.find((item) => item.name === 'server')) {
    app.use(webpackHotServerMiddleware(compiler));
  }

  // Launch WebpackDevServer.
  app.listen(port, (error) => {
    if (error) {
      return console.log(error);
    }

    if (interactive) {
      clearConsole();
    }

    console.log(chalk.cyan('Starting the development server ...'));
    console.log();

    openBrowser(`http://localhost:${port}/`);
  });
};

const run = (port) => {
  runDevServer(port, setupCompiler());
};

// We attempt to use the default port but if it is busy, we offer the user to
// run on a different port. `detect()` Promise resolves to the next free port.
const defaultPort = process.env.PORT || 3000;
detect(defaultPort).then((port) => {
  if (port === defaultPort) {
    run(port);
    return;
  }

  if (interactive) {
    clearConsole();

    const existingProcess = getProcessForPort(defaultPort);
    const question = {
      type: 'confirm',
      name: 'shouldChangePort',
      message: chalk.yellow(
        `Something is already running on port ${defaultPort}.` +
        `${existingProcess ? ` Probably:\n  ${existingProcess}` : ''}`
      ) + '\n\nWould you like to run the app on another port instead?',
      default: true,
    };

    inquirer.prompt(question).then(answer => {
      if (answer.shouldChangePort) {
        run(port);
      }
    });
  } else {
    console.log(chalk.red('Something is already running on port ' + defaultPort + '.'));
  }
})
  .catch((err) => {
    console.error(chalk.red(`Port detection failed! Error:\n${err}`));
  });
