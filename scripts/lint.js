const CLIEngine = require('eslint').CLIEngine;
const paths = require('../config/paths');

const fix = process.argv.includes('--fix');

const cli = new CLIEngine({
  configFile: require.resolve('../.eslintrc.js'),
  fix,
});

try {
  let pathsToLint = process.argv.filter(arg =>
    new RegExp(`^${paths.appSrc}`).test(arg));

  if (pathsToLint.length === 0) {
    pathsToLint = [paths.appSrc];
  }

  const report = cli.executeOnFiles(pathsToLint);
  const formatter = cli.getFormatter();
  const output = formatter(report.results);
  console.log(output);

  if (report.errorCount) {
    process.exit(1);
  }

  if (fix) {
    CLIEngine.outputFixes(report);
  }

  process.exit(0);
} catch (e) {
  console.error(e.message);
  process.exit(1);
}
