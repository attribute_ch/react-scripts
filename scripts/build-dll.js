const webpack = require('webpack');

webpack(require('../config/webpack/webpack.dll'), (err, stats) => {
  if (err) {
    // eslint-disable-next-line no-console
    console.error(err);
    return;
  }

  // eslint-disable-next-line no-console
  console.log(
    stats.toString({
      colors: true, // Shows colors in the console
    })
  );
});
