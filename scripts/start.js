// Load environment variables from .env file.
require('dotenv-extended').config();

const express = require('express');
const path = require('path');
const paths = require('../config/paths');
const clientStats = require(path.join(paths.appBuild, 'stats.json'));

const app = express();
const port = parseInt(process.env.PORT || 3000, 10);

app.set('port', port);
app.set('paths', paths);
app.set('env', process.env);

// eslint-disable-next-line import/no-dynamic-require
const createServerSideRenderer = require(path.join(paths.appBuild, 'server')).default(app);

// Leave it for the implementation to provide the server.
app.use(createServerSideRenderer({ clientStats }));

// Start the app.
// eslint-disable-next-line no-console
app.listen(port, () => console.log(`Server started on port ${port}.`));
